﻿CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL
);

CREATE TABLE recipes (
    id SERIAL PRIMARY KEY,
    submitterId INT NOT NULL REFERENCES users (id),
    name VARCHAR(70) NOT NULL
);

CREATE TABLE quantities (
    id SERIAL PRIMARY KEY,
    description VARCHAR(8) NOT NULL
);

CREATE TABLE ingredients (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    amount VARCHAR(15) NOT NULL,
    quantityId INT NOT NULL REFERENCES quantities (id),
    recipeId INT NOT NULL REFERENCES recipes (id)
);

CREATE TABLE instructions (
    id BIGSERIAL PRIMARY KEY,
    step SMALLINT NOT NULL,
    details VARCHAR(200) NOT NULL,
    recipeId INT NOT NULL REFERENCES recipes (id)
);