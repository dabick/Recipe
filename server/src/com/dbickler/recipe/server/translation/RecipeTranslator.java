package com.dbickler.recipe.server.translation;

import com.dbickler.recipe.server.rest.model.Ingredient;
import com.dbickler.recipe.server.rest.model.Instruction;
import com.dbickler.recipe.server.rest.model.Quantity;
import com.dbickler.recipe.server.rest.model.Recipe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel Bickler
 */
public class RecipeTranslator {

    public Recipe translate(com.dbickler.recipe.server.model.Recipe recipe) {
        Recipe recipe1 =  new Recipe(recipe.getId(), recipe.getSubmitterId(), recipe.getName());
        List<Instruction> instructions = new ArrayList<>(recipe.getInstructions().size());
        for (com.dbickler.recipe.server.model.Instruction instruction : recipe.getInstructions()) {
            instructions.add(new Instruction(instruction.getId(), instruction.getStep(), instruction.getDetails()));
        }
        recipe1.instructions = instructions;
        List<Ingredient> ingredients = new ArrayList<>(recipe.getIngredients().size());
        for (com.dbickler.recipe.server.model.Ingredient ingredient : recipe.getIngredients()) {
            Quantity quantity = new Quantity(ingredient.getQuantity().getId(), ingredient.getQuantity().getDescription());
            ingredients.add(new Ingredient(ingredient.getId(), ingredient.getName(), ingredient.getAmount(), quantity));
        }
        recipe1.ingredients = ingredients;
        return recipe1;
    }
}
