package com.dbickler.recipe.server.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Daniel Bickler
 */
@Entity
@Table(name = "instructions", schema = "public")
public class Instruction {

    @Id
    @GeneratedValue(generator = "instructions_id_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "instructions_id_seq", sequenceName = "instructions_id_seq", allocationSize = 1)
    private Long id;

    @NotNull
    private Integer step;

    @NotNull
    private String details;

    Instruction() {
    }

    public Instruction(Integer step, String details) {
        this.step = step;
        this.details = details;
    }

    public Long getId() {
        return id;
    }

    public Integer getStep() {
        return step;
    }

    public String getDetails() {
        return details;
    }
}
