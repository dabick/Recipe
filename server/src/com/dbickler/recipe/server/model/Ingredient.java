package com.dbickler.recipe.server.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Daniel Bickler
 */
@Entity
@Table(name = "ingredients", schema = "public")
public class Ingredient {

    @Id
    @GeneratedValue(generator = "ingredients_id_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "ingredients_id_seq", sequenceName = "ingredients_id_seq", allocationSize = 1)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String amount;

    @NotNull
    @ManyToOne(optional = false, cascade = {CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "quantityId", referencedColumnName = "id")
    private Quantity quantity;

    Ingredient() {
    }

    public Ingredient(String name, String amount, Quantity quantity) {
        this.name = name;
        this.amount = amount;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAmount() {
        return amount;
    }

    public Quantity getQuantity() {
        return quantity;
    }
}
