package com.dbickler.recipe.server.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Created by Daniel Bickler
 */
@Entity
@Table(name = "recipes", schema = "public")
public class Recipe {

    @Id
    @GeneratedValue(generator = "recipes_id_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "recipes_id_seq", sequenceName = "recipes_id_seq", allocationSize = 1)
    private Integer id;

    @NotNull
    private Integer submitterId;

    @NotNull
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "recipeId", referencedColumnName = "id")
    private Set<Ingredient> ingredients;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "recipeId", referencedColumnName = "id")
    private Set<Instruction> instructions;

    @SuppressWarnings("unused")
    Recipe() {}

    public Recipe(Integer submitterId, String name, Set<Ingredient> ingredients, Set<Instruction> instructions) {
        this.submitterId = submitterId;
        this.name = name;
        this.ingredients = ingredients;
        this.instructions = instructions;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public Integer getSubmitterId() {
        return submitterId;
    }

    public Set<Ingredient> getIngredients() {
        return ingredients;
    }

    public Set<Instruction> getInstructions() {
        return instructions;
    }
}
