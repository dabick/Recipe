package com.dbickler.recipe.server.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Daniel Bickler
 */
@Entity
@Table(name = "quantities", schema = "public")
public class Quantity {

    @Id
    @GeneratedValue(generator = "quantities_id_seq", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "quantities_id_seq", sequenceName = "quantities_id_seq", allocationSize = 1)
    private Integer id;

    @NotNull
    private String description;

    Quantity() {
    }

    public Quantity(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
