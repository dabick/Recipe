package com.dbickler.recipe.server.dao;

import com.dbickler.recipe.server.model.Ingredient;
import com.dbickler.recipe.server.model.Instruction;
import com.dbickler.recipe.server.model.Quantity;
import com.dbickler.recipe.server.model.Recipe;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Daniel Bickler
 */
@Stateless
public class RecipeBO {

    @Inject
    private RecipeDAO dao;

    public Recipe insert(com.dbickler.recipe.server.rest.model.Recipe serviceRecipe) {
        Set<Ingredient> ingredients = new HashSet<>();
        for (com.dbickler.recipe.server.rest.model.Ingredient ingredient : serviceRecipe.ingredients) {
            Quantity quantity = new Quantity(ingredient.quantity.id, ingredient.quantity.description);
            ingredients.add(new Ingredient(ingredient.name, ingredient.amount, quantity));
        }
        Set<Instruction> instructions = new HashSet<>();
        for (com.dbickler.recipe.server.rest.model.Instruction instruction : serviceRecipe.instructions) {
            instructions.add(new Instruction(instruction.step, instruction.details));
        }
        Recipe recipe = new Recipe(serviceRecipe.submitterId, serviceRecipe.name, ingredients, instructions);
        return dao.insert(recipe);
    }

    public Recipe find(Integer id) {
        return dao.find(id);
    }
}
