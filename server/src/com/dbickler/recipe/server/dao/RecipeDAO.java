package com.dbickler.recipe.server.dao;

import com.dbickler.recipe.server.model.Recipe;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Daniel Bickler
 */
public class RecipeDAO {

    @PersistenceContext(unitName = "recipe")
    private EntityManager entityManager;

    public Recipe insert(Recipe recipe) {
        entityManager.persist(recipe);
        return recipe;
    }

    public Recipe find(Integer id) {
        return entityManager.find(Recipe.class, id);
    }
}
