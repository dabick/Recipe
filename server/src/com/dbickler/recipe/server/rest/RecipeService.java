package com.dbickler.recipe.server.rest;

import com.dbickler.recipe.server.dao.RecipeBO;
import com.dbickler.recipe.server.rest.model.Recipe;
import com.dbickler.recipe.server.translation.RecipeTranslator;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.*;

/**
 * Created by Daniel Bickler
 */
@Path("/recipes")
@Produces("application/json")
public class RecipeService {

    @EJB
    private RecipeBO recipeBO;

    @Inject
    private RecipeTranslator recipeTranslator = new RecipeTranslator();

    @GET
    @Path("{id}")
    public Recipe getRecipe(@PathParam("id") Integer id) {
        return recipeTranslator.translate(recipeBO.find(id));
    }

    @POST
    public Recipe postRecipe(Recipe recipe) {
        return recipeTranslator.translate(recipeBO.insert(recipe));
    }

    @GET
    @Path("/test")
    public String test() {
        return "Hello World";
    }

}
