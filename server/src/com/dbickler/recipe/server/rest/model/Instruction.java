package com.dbickler.recipe.server.rest.model;

/**
 * Created by Daniel Bickler
 */
public class Instruction {

    public Long id;
    public Integer step;
    public String details;
    public Integer recipeId;

    public Instruction() {
    }

    public Instruction(Long id, Integer step, String details) {
        this.id = id;
        this.step = step;
        this.details = details;
    }
}
