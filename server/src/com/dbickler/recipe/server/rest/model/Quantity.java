package com.dbickler.recipe.server.rest.model;

/**
 * Created by Daniel Bickler
 */
public class Quantity {

    public Integer id;
    public String description;

    public Quantity() {
    }

    public Quantity(Integer id, String description) {
        this.id = id;
        this.description = description;
    }
}
