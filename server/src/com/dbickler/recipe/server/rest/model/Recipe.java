package com.dbickler.recipe.server.rest.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Daniel Bickler
 */
@XmlRootElement
public class Recipe {

    public Integer id;
    public Integer submitterId;
    public String name;
    public List<Instruction> instructions;
    public List<Ingredient> ingredients;

    Recipe() {}

    public Recipe(Integer id, Integer submitterId, String name) {
        this.id = id;
        this.submitterId = submitterId;
        this.name = name;
    }
}
