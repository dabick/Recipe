package com.dbickler.recipe.server.rest.model;

/**
 * Created by Daniel Bickler
 */
public class Ingredient {

    public Long id;
    public String name;
    public String amount;
    public Quantity quantity;
    public Integer recipeId;

    public Ingredient() {
    }

    public Ingredient(Long id, String name, String amount, Quantity quantity) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.quantity = quantity;
    }
}
